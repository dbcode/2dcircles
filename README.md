# Site 2D circles 

## Mise en place du container

### téléchargement fichiers

Aller dans le répertoire ou le projet va être téléchargé:

`cd 2dcircles`

télécharger le repository Git:

`git clone https://gitlab.com/dbcode/2dcircles.git .`

### Contruction et execution du containeur

`docker-compose up --build -d`

### accès console containeur

`docker exec -i -t 2dcircles_web_1 bash`

adapter la partie `2dcircles_web_1` avec le nom du container sur votre station.

### installation node_modules

dans la console du containeur

`npm install` ou `npm update`

### execution taches gulp

execution taches dans gulpfile

`npx gulp default` ou `gulp default`

restart server.js 

`docker-compose stop` and`docker-compose start`

ou

modifier server.js (p.e. ajouter un espace)

### ouvrir la page dans un navigateur

accès serveur sur `http://localhost:8067/`


## Structure 

Pour modifier les parametres de bases des serveurs, il faut éditer le fichier **./docker-compose.yml**

Le dossier **./src** contient le repertoire du projet web


## Lien par défaut

http://localhost:8067


## Liste des commandes 

#### Démarrer les containers 

`docker-compose start`

#### Stopper les containers 

`docker-compose stop`

#### Supprimer les containers 

`docker-compose down -v`

## Docker-compose.yml

Ce fichier permet d'éditer certains des parametres des containers.

### Principaux paramètre

#### Ports

Permet d'éditer les ports utilisés par Docker. Ne modifiez que les premiers chiffre (par exemple **8067**:8080 en **8080**:8080) 

#### Volumes

Permet de faire un lien entre le systeme de fichier du container et de votre machine. Ne modifiez que la première valeur (Par exemple **./src**:/src en **./src/dev**:/src). Celle-ci doit correspondre a un répertoire exsitant contenant le projet Web.

### Tableau des paramètres

|  Nom          	| Exemple             	| Explication                                	|
|----------------:	|---------------------	|--------------------------------------------------------------------------------------	|
| **build**       	| context: ./       	| Paramètres de personnalisation techniques des container  	|
| **ports**       	| 8067:8080            	| Permet de définir les ports ouverts du container          |
| **volumes**     	| ./src:/src         	| Lien entre le système de fichier du container et de votre machine|