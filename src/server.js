const browserSync = require('browser-sync').create()

browserSync.init({
    server: "/src/prod",
    index: "index.html",
    baseDir: "./", 
    port: 8080,
    open: false,
    notify: false
});