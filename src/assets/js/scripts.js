// time constant (ms)
const INTERVAL = 10;

// initial conditions
const START_PARTICLES = 10;
const MAX_PARTICLES = 40;

// damage system constants
const EXPLOSIONS = 1;
const DEATH = 1;
const DAMAGE_PER_HIT = 101;
const TOTAL_DAMAGE = 400;
const HEALING_RATE = 1;
const IMPACT_ANIMATION = 1;
const IMPACT_ANIMATION_ITERATION_TIME = 400; //ms
const IMPACT_ANIMATION_ITERATION_NUMBER = 20;

// shadow control
const SHADOW_MODE = 1; // 0 = none, 1 = sun shadow, 2 = fixed shadow

// common particle class
class BaseParticle {
    constructor(container, id) {
        this.id = id + "_" + Math.floor(Math.random() * 10000000);
    }

    // style the particle shadow
    styleShadow(time) {
        if (SHADOW_MODE == 0) {
            this.elem.style.boxShadow = "unset";
        }
        else if (SHADOW_MODE == 1) {
            let xShadow = 10;
            let yShadow = 10;
            let intensity = 0;
            time = time % 1000;
            if (time > 500) {
                this.elem.style.boxShadow = "unset";
            }
            else {
                xShadow = Math.round((time - 250) / 4);
                yShadow = Math.round((time - 250) / 20);
                intensity = (time / 2 - 125) * (time / 2 - 125);
                this.elem.style.boxShadow = xShadow + "px " + yShadow + "px " + (intensity / 100) + "px " + (intensity / 1000) + "px darkgrey";
            }
        }
        else if (SHADOW_MODE == 2) {
            this.elem.style.boxShadow = "5px 5px 10px 5px darkgrey";
        }
        else {
            this.elem.style.boxShadow = "unset";
        }
    }

    // remove element from DOM
    delete(container) {
        container.removeChild(this.elem);
    }
}

// exploding particle class
class ExplodingParticle extends BaseParticle {
    constructor(container, particle) {
        super(container, particle.id);
        this.pos_x = particle.pos_x;
        this.pos_y = particle.pos_y;
        this.radius = particle.radius;

        this.elem = document.createElement('div');
        this.elem.classList.add('particle');
        container.appendChild(this.elem);

        this.elem.style.backgroundColor = "transparent";
        this.elem.style.width = this.radius * 2 + "px";
        this.elem.style.height = this.radius * 2 + "px";
        this.elem.style.borderRadius = Math.round(this.radius) + "px";
        this.elem.style.top = Math.round(this.pos_x) + "px";
        this.elem.style.left = Math.round(this.pos_y) + "px";
        this.elem.style.opacity = 1;
        this.elem.style.border = this.radius + "px solid red";
    }

    // draw next step of explosion animation
    explode() {
        this.pos_x -= 1;
        this.pos_y -= 1;
        this.radius += 1;
        this.elem.style.backgroundColor = "transparent";
        this.elem.style.width = this.radius * 2 + "px";
        this.elem.style.height = this.radius * 2 + "px";
        this.elem.style.borderRadius = Math.round(this.radius) + "px";
        this.elem.style.top = Math.round(this.pos_x) + "px";
        this.elem.style.left = Math.round(this.pos_y) + "px";
        this.elem.style.opacity = this.elem.style.opacity - 0.002;
        this.elem.style.zIndex = 10;
    }

    animationCompleted() {
        return this.elem.style.opacity < 0.001;
    }
}

// moving particle class
class Particle extends BaseParticle {
    constructor(container, id, particles) {
        super(container, id);

        //generate initial parameters (avoid overlapping with existing particles) ! could infinite loop
        let loop = true;
        while (loop) {
            loop = false;
            this.generateInitialValues(container);
            for (let i = 0; i < particles.length; i++) {
                if (this.touches(particles[i])) {
                    loop = true;
                }
            }
        }
        this.generateElement(container);
    }

    // randomly initialize position, size and speed
    generateInitialValues(container) {
        this.container_height = container.scrollHeight;
        this.container_width = container.offsetWidth;
        this.radius = 10 + Math.floor(Math.random() * 40);
        this.mass = this.radius * this.radius; // 2D mass
        this.x_min = 0;
        this.y_min = 0;
        this.x_max = this.container_height - this.radius * 2;
        this.y_max = this.container_width - this.radius * 2;
        // current pos
        this.pos_x = Math.floor(Math.random() * (this.x_max));
        this.pos_y = Math.floor(Math.random() * (this.y_max));
        // initial pos
        this.i_pos_x = this.pos_x;
        this.i_pos_y = this.pos_y;
        // candidate pos
        this.c_pos_x = this.pos_x;
        this.c_pos_y = this.pos_y;

        // current speed
        if (Math.floor(Math.random() * 2) == 1) {
            this.x_dir = Math.floor(Math.random() * 2) + 1;
        }
        else {
            this.x_dir = -1 * (Math.floor(Math.random() * 2) + 1);
        }

        if (Math.floor(Math.random() * 2) == 1) {
            this.y_dir = Math.floor(Math.random() * 2) + 1;
        }
        else {
            this.y_dir = -1 * (Math.floor(Math.random() * 2) + 1);
        }
        // initial speed
        this.i_x_dir = this.x_dir;
        this.i_y_dir = this.y_dir;
        // candidate speed
        this.c_x_dir = this.x_dir;
        this.c_y_dir = this.y_dir;

        // impact info
        this.impact_x_dir = 0;
        this.impact_y_dir = 0;
        this.impactCountdown = 0;
    }

    // initialize dom element
    generateElement(container) {
        this.elem = document.createElement('div');
        this.elem.id = "particle_" + this.id;
        this.elem.classList.add('particle');
        let randomColor = '#' + parseInt(Math.random() * 255).toString(16).padStart(2, '0') + parseInt(Math.random() * 255).toString(16).padStart(2, '0') + parseInt(Math.random() * 255).toString(16).padStart(2, '0');
        this.elem.style.backgroundColor = randomColor;
        this.elem.style.width = this.radius * 2 + "px";
        this.elem.style.height = this.radius * 2 + "px";
        this.elem.style.borderRadius = Math.round(this.radius) + "px";
        this.elem.style.zIndex = 15;
        let transitionTime = IMPACT_ANIMATION_ITERATION_TIME/1000;
        this.elem.style.transition = "transform " + transitionTime+"s";

        this.elem.dataset.explode = 0;
        this.elem.dataset.damagecounter = 0;
        this.elem.dataset.impact = 0;
        this.elem.onclick = function () { toDel.push(this.id); }
        container.appendChild(this.elem);
    }

    // compute next position
    preMove() {
        this.i_pos_x = this.pos_x;
        this.i_pos_y = this.pos_y;
        this.c_pos_x = this.pos_x + this.x_dir;
        this.c_pos_y = this.pos_y + this.y_dir;
        this.i_x_dir = this.x_dir;
        this.i_y_dir = this.y_dir;
        this.c_x_dir = this.x_dir;
        this.c_y_dir = this.y_dir;
    }

    // check if particles go out of container bounds
    boundCollision() {
        if (this.c_pos_x >= this.x_max - 1) {
            this.x_dir = -1 * Math.abs(this.x_dir);
            this.c_x_dir = this.x_dir;
        }

        if (this.c_pos_x <= 1) {
            this.x_dir = Math.abs(this.x_dir);
            this.c_x_dir = this.x_dir;
        }

        if (this.c_pos_y >= this.y_max - 1) {
            this.y_dir = -1 * Math.abs(this.y_dir);
            this.c_y_dir = this.y_dir;
        }

        if (this.c_pos_y <= 1) {
            this.y_dir = Math.abs(this.y_dir);
            this.c_y_dir = this.y_dir;
        }
    }

    // check if particle collides and compute collision
    particleCollision(particles) {
        for (let i = 0; i < particles.length; i++) {
            if (this.id != particles[i].id) {
                if (this.touches(particles[i])) {
                    this.impact = 1;
                    this.react(particles[i]);
                    this.doImpact();
                }
            }
        }
    }

    // check if 2 particles collide
    touches(particle) {
        let center_x = this.c_pos_x + this.radius;
        let center_y = this.c_pos_y + this.radius;
        let p_center_x = particle.c_pos_x + particle.radius;
        let p_center_y = particle.c_pos_y + particle.radius;

        let d = Math.sqrt((center_x - p_center_x) * (center_x - p_center_x) + (center_y - p_center_y) * (center_y - p_center_y));

        if (d <= this.radius + particle.radius) {
            return true;
        }
        else {
            return false;
        }
    }

    // compute speeds after collision
    react(particle) {
        // compute new speeds
        let A = (this.mass - particle.mass) / (this.mass + particle.mass);
        let B = 2 / (this.mass + particle.mass);
        this.c_x_dir = A * this.x_dir + B * particle.mass * particle.x_dir;
        this.c_y_dir = A * this.y_dir + B * particle.mass * particle.y_dir;

        // store impact direction
        let speedModule = this.c_x_dir * this.c_x_dir + this.c_y_dir * this.c_y_dir;
        this.impact_x_dir = this.impact_x_dir * this.impactCountdown / IMPACT_ANIMATION_ITERATION_TIME + (this.c_x_dir * this.c_x_dir) / speedModule;
        this.impact_y_dir = this.impact_y_dir * this.impactCountdown / IMPACT_ANIMATION_ITERATION_TIME + (this.c_y_dir * this.c_y_dir) / speedModule;
    }

    // compute new damage and impact animation after impact
    doImpact() {
        this.elem.dataset.damagecounter = Number(this.elem.dataset.damagecounter) + DAMAGE_PER_HIT;
        if (this.elem.dataset.damagecounter > TOTAL_DAMAGE) {
            this.elem.dataset.damagecounter = TOTAL_DAMAGE;
            this.elem.dataset.explode = 1;
        }
        this.impactCountdown = IMPACT_ANIMATION_ITERATION_TIME;
    }

    // set new position
    move() {
        if (this.impact == 1) {
            this.c_pos_x = this.i_pos_x;
            this.c_pos_y = this.i_pos_y;
            this.impact = 0;
        }
        this.pos_x = this.c_pos_x;
        this.pos_y = this.c_pos_y;
        this.x_dir = this.c_x_dir;
        this.y_dir = this.c_y_dir;
        this.elem.style.top = Math.round(this.pos_x) + "px";
        this.elem.style.left = Math.round(this.pos_y) + "px";
        this.styleMove();
    }

    // draw at position
    styleMove() {
        if (this.elem.dataset.damagecounter > 0) {
            this.elem.dataset.damagecounter -= HEALING_RATE;
            if (this.elem.dataset.damagecounter < 1) {
                this.elem.dataset.damagecounter = 0;
                this.elem.style.border = "0";
            }
            else {
                let border = Math.floor(Number(this.elem.dataset.damagecounter) * this.radius / (TOTAL_DAMAGE - DAMAGE_PER_HIT));
                this.elem.style.border = border + "px solid red";
            }
        }
        else {
            this.elem.style.border = "0";
        }

        if (IMPACT_ANIMATION == 1 && this.impactCountdown > 0) {
            let timeAdapter = IMPACT_ANIMATION_ITERATION_TIME / INTERVAL;
            if (this.impactCountdown % timeAdapter == 0) {
                let iteration = Math.floor(this.impactCountdown / timeAdapter);
                let isOddIteration = iteration % 2;
                let magnitute = this.impactCountdown / IMPACT_ANIMATION_ITERATION_TIME;
                if (isOddIteration == 1) {
                    magnitute = -1 * magnitute;
                }

                let rotx = Math.floor(45 * this.impact_x_dir * magnitute);
                let roty = Math.floor(45 * this.impact_y_dir * magnitute);
                this.elem.style.transform = "rotateX(" + rotx + "deg) rotateY(" + roty + "deg)";
            }
            this.impactCountdown = this.impactCountdown - ((IMPACT_ANIMATION_ITERATION_TIME / INTERVAL) / IMPACT_ANIMATION_ITERATION_NUMBER);
            if (this.impactCountdown < 1) {
                this.impactCountdown = 0;
                this.elem.style.transform = "rotateX(0deg) rotateY(0deg)";
                this.impact_x_dir = 0;
                this.impact_y_dir = 0;
            }
        }
    }

    fullHeal() {
        this.elem.dataset.explode = 0;
        this.elem.dataset.damagecounter = 0;
        this.elem.dataset.explode = 0;
    }

    gotLethalDamage() {
        return this.elem.dataset.explode == 1;
    }

    setToExplode() {
        this.elem.dataset.explode = 1;
    }

    // get particle energy value
    getEnergy() {
        return this.getSpeed() * this.getSpeed() * this.mass;
    }

    // get particle speed value (pixels/s)
    getSpeed() {
        return Math.sqrt(this.x_dir * this.x_dir + this.y_dir * this.y_dir) * 1000 / INTERVAL;
    }

    // get particle momentum
    getMomentum() {
        return this.getSpeed() * this.mass;
    }
}

// global variables
var addQueue = 0;
var particlesIdIncrement = 0;
var particlesNumber = 0;
var particles = [];
var explodingParticles = [];
var toDel = [];
var container = document.querySelector('#particle_container');
var maxSpeedMeter = document.querySelector('#max_speed_meter');
var energyMeter = document.querySelector('#energy_meter');
var particlesNumberMeter = document.querySelector('#particles_number');
var avgSpeedMeter = document.querySelector('#avg_speed_meter');
var totalMomentumMeter = document.querySelector('#total_momentum_meter');

document.querySelector('#add_particle').onclick = function () { addQueue += 1; };

function myMove() {
    // create a few initial particles
    /*
    for (let i = 0; i < START_PARTICLES; i++) {
        particles[i] = new Particle(container, particlesIdIncrement, particles);
        particlesNumber += 1;
        particlesIdIncrement += 1
    }*/
    var iteration = 0;
    var id = setInterval(frame, INTERVAL);
    function frame() {
        if (iteration == -1) {
            clearInterval(id);
        }
        else {
            iteration++;
            // react to add
            while (addQueue > 0) {
                if (particlesNumber < MAX_PARTICLES) {
                    particles.push(new Particle(container, particlesIdIncrement, particles));
                    particlesNumber += 1;
                    particlesIdIncrement += 1
                }
                addQueue -= 1;
            }
            // compute next position
            for (let i = 0; i < particles.length; i++) {
                particles[i].preMove();
            }
            // compute bound collision
            for (let i = 0; i < particles.length; i++) {
                particles[i].boundCollision();
            }
            // compute particle collision   
            for (let i = 0; i < particles.length; i++) {
                particles[i].particleCollision(particles);
            }
            // draw new position
            for (let i = 0; i < particles.length; i++) {
                particles[i].move();
            }
            // react to delete
            if (EXPLOSIONS == 1) {
                // explosion on click
                for (let i = 0; i < toDel.length; i++) {
                    for (let j = 0; j < particles.length; j++) {
                        if ("particle_" + particles[j].id == toDel[i]) {
                            particles[j].setToExplode();
                        }
                    }
                    toDel.shift();
                }
            }
            else {
                // delete without explosion
                for (let i = 0; i < toDel.length; i++) {
                    for (let j = 0; j < particles.length; j++) {
                        if ("particle_" + particles[j].id == toDel[i]) {
                            particles[j].delete(container);
                            particlesNumber -= 1;
                        }
                    }
                    particles = particles.filter(function (value, index, arr) { return value.id != toDel[i]; });
                    i = 1 - 1;
                    toDel.shift();
                }
            }

            // explode 1. create ExplodingParticle
            if (EXPLOSIONS == 1) {
                for (let i = 0; i < particles.length; i++) {
                    if (particles[i].gotLethalDamage()) {
                        explodingParticles.push(new ExplodingParticle(container, particles[i]));
                    };
                }
            }

            // explode 2. remove standard exploding particle
            for (let i = 0; i < particles.length; i++) {
                if (particles[i].gotLethalDamage()) {
                    if (DEATH == 1) {
                        particles[i].delete(container);
                        particles = particles.filter(function (value, index, arr) { return value.id != particles[i].id; });
                        particlesNumber -= 1;
                        i = 1 - 1;
                    }
                    else {
                        particles[i].fullHeal();
                    }
                };
            }

            // explode 3. remove ExplodingParticle when animation completed
            for (let i = 0; i < explodingParticles.length; i++) {
                explodingParticles[i].explode();
                if (explodingParticles[i].animationCompleted()) {
                    explodingParticles[i].delete(container);
                    explodingParticles = explodingParticles.filter(function (value, index, arr) { return value.id != explodingParticles[i].id; });
                    i = 1 - 1;
                }
            }

            // shadowstyle
            for (let i = 0; i < particles.length; i++) {
                particles[i].styleShadow(iteration);
            }

            for (let i = 0; i < explodingParticles.length; i++) {
                explodingParticles[i].styleShadow(iteration);
            }

            // upate console
            let totalEnergy = 0;
            let maxSpeed = 0;
            let avgSpeed = 0;
            let totalMomentum = 0;
            for (let i = 0; i < particles.length; i++) {
                maxSpeed = Math.max(maxSpeed, particles[i].getSpeed());
                totalEnergy += particles[i].getEnergy();
                totalMomentum += particles[i].getMomentum();
                avgSpeed += particles[i].getSpeed() / particlesNumber;
            }

            maxSpeedMeter.innerHTML = maxSpeed.toFixed(0);
            energyMeter.innerHTML = totalEnergy.toFixed(0);
            particlesNumberMeter.innerHTML = particlesNumber;
            avgSpeedMeter.innerHTML = avgSpeed.toFixed(0);
            totalMomentumMeter.innerHTML = totalMomentum.toFixed(0);
        }
    }
}

function initParticles() {
    for (let i = 0; i < START_PARTICLES; i++) {
        particles[i] = new Particle(container, particlesIdIncrement, particles);
        particlesNumber += 1;
        particlesIdIncrement += 1
    }

    for (let i = 0; i < particles.length; i++) {
        particles[i].move();
    }
}

document.querySelector('#startButton').onclick = function () { 
    this.parentElement.parentElement.removeChild(this.parentElement);
    myMove();
};

initParticles();