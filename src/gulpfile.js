var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var uglifycss = require('gulp-uglifycss');


gulp.task('pack-js', function () {
    return gulp.src(['assets/js/scripts.js'])
        .pipe(concat('scripts.min.js'))
        .pipe(gulp.dest('assets/js'));
});

gulp.task('pack-css', function () {
    return gulp.src(['assets/css/style.css'])
        .pipe(concat('style.min.css'))
        .pipe(gulp.dest('assets/css'));
});

gulp.task('uglify-js', function () {
    return gulp.src(['assets/js/scripts.min.js'])
        .pipe(uglify())
        .pipe(gulp.dest('prod/js'));
});

gulp.task('uglify-css', function () {
    return gulp.src(['assets/css/style.min.css'])
        .pipe(uglifycss({
            "maxLineLen": 80,
            "uglyComments": true
        }))
        .pipe(gulp.dest('prod/css'));
});


gulp.task('default', gulp.series('pack-js', 'pack-css', 'uglify-js','uglify-css'));